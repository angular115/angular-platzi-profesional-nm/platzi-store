# Curso Profesional de Angular 8

## Proyecto Platzi Shop

Instruido por:

Nicolas Molina

Descripción:

Implementación del proyecto Platzi Shop. Aplicando los patrones de Smart and Dumb Components para crear tu propia aplicación web.

![PlatziStorePro](https://i.imgur.com/kwejJQ5.png)
![PlatziStorePro](https://i.imgur.com/Poz3EWD.png)

## Nota 🏁 (Consiguraciones):

El proyecto en este punto ya esta en la version 12 (originalmente el del repositorio del profe empieza con la version 8), pero para que no tengan problemas dejo este punto en una rama. Luego de descargarlo o clonarlo, ejecutar:

- Ejecutar (si no tienes instalado Angular):
  `npm i -g @angular/cli@latest`
- Luego:
  `npm install`

### Cambios por si alguno decargo el repositorio del profe con la version 8:

- Ejecutar:
  `npm i -g @angular/cli@latest`
- Luego:
  `ng update`

  Hago un parentesis acá, el coamndo del profe `ng update @angular/cli @angular/core` no les va a funcionar (eso me paso), les pedira que actualicen a la versión 9, de ahí siguen los pasos, recomiendo que actualicen uno por uno.

  Luego de esto, para algunos problemas que les apareceran, realicen los siguientes cambios,

- Cambie en angular.json:
  ./node_modules/swiper/dist/css/swiper.css
  por
  ./node_modules/swiper/swiper-bundle.css

- Asimismo, quitar `auth` en varios archivos:
  `return this.af.auth.createUserWithEmailAndPassword(email, password);`
  ...
  y queda asi
  `return this.af.createUserWithEmailAndPassword(email, password);`
  ...
- Por ultimo, realizar el siguiente cambio estas importaciones:

```
// import { AngularFireModule } from '@angular/fire';
// import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
```

(Si hasta este punto no les funciona, recomiendo, mejor clonar este repositorio en lugar de la del profe para poder continuar con el curso.)

## Sección HTTPCLIENT 🚩 ():

### Comandos:

- `npm install --save @sentry/angular @sentry/tracing`
- `npm i file-saver`

## Sección Puesta-Produccion 🚩 ():

### Comandos:

- `docker pull nginx:alpine`
- `docker run -d -p 8080:80 nginx:alpine`
- `docker ps` // Ver nuestros contenedores corriendo
- `docker stop COONTAINERID`
- `docker run -d -p 8080:80 -v D:/\'Material Educativo'/\Angular/\Angular_PlatziProfesional_nm/\platzi-store-pro/\dist/\platzi-store-v7:/usr/share/nginx/html nginx:alpine`//Este comando funciono en bash
  Este último si no les funciona, pueden correr lo siguiente:
- `docker build . -t platzi-store:latest`//tener archivo Dockerfile creado
- `docker run -d -p 8080:80 platzi-store:latest`
  Finalmente:
- `docker stop COONTAINERID`

## Datos para ingresar al Admin 🚩 :

- Usuario: nicolas@nicolas.com
- Contraseña 123456

<!-- Comandos que probe para correr:
- `docker run -d -p 8080:80 -v $(pwd)/dist/platzi-store-v7/:/usr/share/nginx/html nginx:alpine
- `docker run -d -p 8080:80 -v /d/Material Educativo/Angular/Angular_PlatziProfesional_nm/platzi-store-pro/dist/platzi-store-v7/:/usr/share/nginx/html nginx:alpine
- `docker run -d -p 8080:80 -v //d//Material Educativo//Angular//Angular_PlatziProfesional_nm//platzi-store-pro//dist//platzi-store-v7//:/usr/share/nginx/html nginx:alpine
- `docker run -d -p 8080:80 -v /\d/\Material Educativo/\Angular/\Angular_PlatziProfesional_nm/\platzi-store-pro/\dist/\platzi-store-v7:/usr/share/nginx/html nginx:alpine
<!-- -->

<!-- Sentry:
Cambiar:
import \* as Sentry from '@sentry/browser';
Por:
import \* as Sentry from "@sentry/angular";

Correr Coverage:
ng test --watch=false --codeCoverage=true --> -->

<!-- Api products:
https://platzi-store.herokuapp.com/products -->
