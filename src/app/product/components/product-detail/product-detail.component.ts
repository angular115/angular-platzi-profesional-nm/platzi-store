import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ProductsService } from '@core/services/products/products.service';
import { Product } from '@core/models/product.model';

import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product$: Observable<Product>;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    this.product$ = this.route.params
    .pipe(
      switchMap((params: Params) => {
        return this.productsService.getProduct(params.id);
      })
    );
  }

  createProduct() {
    const newProduct: Product = {
      id: '222',
      title: 'nuevo desde angular',
      image: 'assets/images/banner-1.jpg',
      price: 3000,
      description: 'nuevo producto'
    };
    this.productsService.createProduct(newProduct)
    .subscribe(product => {
      console.log(product);
    });
  }

  updateProduct() {
    const updateProduct: Partial<Product> = {
      price: 555555,
      description: 'edicion titulo'
    };
    this.productsService.updateProduct('2', updateProduct)
    .subscribe(product => {
      console.log(product);
    });
  }

  deleteProduct() {
    this.productsService.deleteProduct('222')
    .subscribe(rta => {
      console.log(rta);
    });
  }

  getRandomUsers() { // Prueba e tipado en peticiones
    this.productsService.getRandomUsers()
    .subscribe(users => {
      console.log(users);
    },//bien
    error => {
      console.error(error);
    }//mal
    );
  }

  getFile() { //lectura de un archivo 
    this.productsService.getFile()
    .subscribe(content => {
      console.log(content);
      const blob = new Blob([content], {type: 'text/plain;charset=utf-8'});
      FileSaver.saveAs(blob, 'hello world.txt');
    });
  }


}

//Esto trabaja con doble suscribe

// @Component({
// ...
// })
// export class ProductDetailComponent implements OnInit {

//   product: Product;

//   constructor(
//...
//   ) { }

//   ngOnInit() {
//     this.route.params.subscribe((params: Params) => {
//       const id = params.id;
//       this.fetchProduct(id);
//       // this.product = this.productsService.getProduct(id);
//     });
//   }

//   fetchProduct(id: string) {
//     this.productsService.getProduct(id)
//     .subscribe(product => {
//       this.product = product;
//     });
//   }

//   createProduct() {
//...
//   }

//   updateProduct() {
//...
//   }

//   deleteProduct() {
//...
//   }

// }
